# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

```plantuml
@startmindmap
+[#Chocolate] Programación Declarativa.\n Orientaciones y pautas para el estudio (1998, 1999, 2001)
++_ Que es 
+++[#Crimson] Un estilo de programacion que surgue a travez de la programacion clasica,\n que consiste en que las tareas rutinarias se dejan al compilador.
++++_ Tiene 
+++++[#white] proposito 
++++++_ que es 
+++++++[#orange] liberarse de las asignaciones 
+++++++[#orange] liberarse de detallar especificamente el control de la gestion de memoria
+++++++[#orange] utilizar otros recursos expresivos
+++++++[#orange] reducir los riesgos de cometer errores haciendo programas 
++++++++_ para 
+++++++++[#orange] especificar el programa a un nivel mas cercano a la forma de pensar del programador 
+++++++[#orange] liberarse de la arquitectura de von neumann
+++++[#lime] donde utilizar 
++++++_ como en 
+++++++[#yellow] inteligencia artificial 
+++++++[#yellow] trabajos de los arquitectos  
+++++++[#yellow] bases de datos 
+++++[#white] Principios
++++++_ como 
+++++++[#Gold] liberarse de las asignaciones 
+++++++[#Gold] liberarse de tener que detallar el control de la gestion de mememoria en el ordenador
+++++++[#Gold] utilizar otros recursos expresivos 
+++++[#tan] caracteristicas 
++++++_ tales como
+++++++[#turquoise] se tienen programas mas cortos 
+++++++[#turquoise] el tiempo de desarrollo es menor al igual que el tiempo de depuracion
+++++++[#turquoise]  los programas son mas faciles de realiza y depurar
+++++++[#turquoise] el compilador es el que toma las desiciones
+++++++[#turquoise] se tiene el conocimiento de las caracteristicas mas relevantes de los lenguajes actuales
+++++++[#turquoise] es fundamental para aprender a programar bien 
+++++++[#turquoise] se programa de forma sencilla y placentera   
+++++[#white] tipos de Lenguajes
++++++_ tales como 
+++++++[#Plum] Prolog
++++++++[#Plum] tiene relacion con la programacion logica 
+++++++[#Plum] erlang
+++++++[#Plum] lisp 
++++++++[#Plum] tiene relacion con la programacion funcional
+++++++[#Plum] haskell 
++++++++[#Plum] es el lenguaje estandar en este tipo de paradigmas 
+++++[#Pink] Variantes
++++++_ como
+++++++[#Plum] programacion funcional 
++++++++_ que se hace 
+++++++++[#red] recurrir al lenguaje que usan los matematicos\n en particular al lenguje que se usa para escribir funciones 
++++++++++_ atributos 
+++++++++++[#white] la existencia de las funciones de orden superior
++++++++++++[#yellow] consiste en que la entrada de un programa puede ser otro programa
++++++++++++[#yellow] no hay una distincion entre datos y programas 
+++++++++++[#white] la evaluacion perezosa 
++++++++++++[#yellow] consiste en que el ordenador solo ejecuta aquello que es necsario obligatorio para darnos respuesta
+++++++++++[#white] se usa el lenguaje lisp
+++++++++++[#white] se basa en el modelo de la logica y de la demostracion automatica
+++++++++++[#white] se programa mas facil
+++++++++++[#white] el compilador resuelve todo
+++++++++++[#white] se tiene un mayor panorama 
+++++++++++[#white] es muy util para especificar los programas
+++++++[#orange] programacion logica
++++++++_ que hace
+++++++++[#violet] es acudir a la logica de predicados de primer orden
++++++++++ atributos
+++++++++++_ como
++++++++++++[#azure] no hay una distincion entre los  datos
+++++++++++++_ de
++++++++++++++  entrada 
++++++++++++++  salida 
++++++++++++[#azure] ser mas declarativos 
++++++++++++[#azure] no les importa mucho el orden 
++++++++++++[#azure] su lenguaje estandar es el prolog
++++++++++++[#azure] se tiene relacion entre los objetos 
++++++++++++[#azure] se opera mediante algoritmos de resolucion
++++++++++[#orange] desventajas
+++++++++++_ como
++++++++++++[#green] depende mucho del tipo de programas que uno quiere hacer
++++++++++++[#green] son  ideas abstractas y es complejo llevarla a la practica   
+++++[#white] surge 
++++++_ como 
+++++++[#yellow] reaccion a los problemas de la programacion clasica  
++++++++[#yellow] generaba problemas 
+++++++++_ tales como 
++++++++++[#lime] se daban demasiado detalles para la programacion
++++++++++[#lime] se creaban cuello de botellas 
++++++++++[#lime] se generaba muchas especificaciones que eran innecesarias para el programa
@endmindmap
```
# Lenguaje de Programación Funcional (2015)

```plantuml
@startmindmap
+[#Gold] Lenguaje de Programación Funcional
++[#Yellow] es un paradigma que es basado en el uso de funciones matematicas
+++_ tiene
++++[#pink] ventajas
+++++_ tales 
++++++[#orange] los programas solo dependen de los parametros de entrada
++++++[#orange] se pueden paralizar sin ningun problema
++++++[#orange] permite tener codigos muy breves
++++++[#orange] el orden no es relevante en lo absoluto
++++++[#orange] sirve para diseñar programas que luego se implementen en otros lenguajes 
++++[#red] bases 
+++++_ son
++++++[#azure] todo lo que se puede hacer en uno se puede hacer en todos 
++++++[#azure] se basa en funciones puramente matematicas
++++++[#azure] se basa en la arquitectura de von neumann
++++++[#azure] logica convinatoria 
++++[#silver] desventajas
+++++_ como
++++++[#White] se pierde el concepto de variable que uno puede modificar a lo largo del computo
++++++[#white] no se usan los bucles 
++++[#violet] caracteristicas
+++++_ como 
++++++[#plum] todo gira alrededor de las funciones
+++++++[#Lightgray] se denomina ciudadanos de primera clase 
+++++++[#Lightgray] las funciones pueden por todas partes 
+++++++[#Lightgray] incluso pueden ser parametros 
++++++[#plum] transparencia referencial
+++++++[#gray] los resultados de las funciones son independientes del orden en el que se realizen los calculos 
++++++[#plum] se usa la recursividad 
++++++[#plum] no son necesarias las variables 
++++++[#plum] todas la funciones pueden devolver funciones
++++++[#plum] las funciones no necesitan ser nombradas de forma explicita
++++++[#plum] currificacion 
+++++++[#gray] es una funcion que tiene varios parametos de entrada que no solo devuelve una salida
++++++[#plum] aplicacion parcial
+++++++[#Lightgray] es cuando se devuelven diferentes funciones
++++++[#plum] las constantes equivalen a funciones 
++++++[#plum] se pierde el estado del computo
++++[#white] como se evaluan las funciones 
+++++_ con 
++++++[#crimson] evaluacion perezosa 
+++++++[#Lightgray] recordar lo ya evaluado para no volverlo a evaluar 
+++++++[#Lightgray] nunca trabaja de mas 
++++++[#crimson] evaluacion impaciente 
+++++++[#Lightgray] evalua desde dentro hacia afuera 
++++++[#crimson] evaluacion no extricta 
+++++++[#Lightgray] evaluar desde fuera hacia dentro
++++++[#crimson] evaluacion costosa
++++[#lime] se aplica
+++++_ a
++++++[#turquoise] juegos de arcade 
++++++[#turquoise] sudoku
++++++[#turquoise] diferentes situaciones 
+++++++_ como
++++++++[#brown] desarrollar word
@endmindmap
```

